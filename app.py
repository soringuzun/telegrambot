from flask import Flask, request, Response, jsonify, render_template, send_file
from dotenv import load_dotenv
import redis
import requests
import os
import time
import datetime
import random
import json

app = Flask(__name__)

load_dotenv()

TOKEN = os.getenv('TELEGRAM_TOKEN')
JENKINS_TOKEN = os.getenv('JENKINS_TOKEN')
URL = "https://api.telegram.org/bot" + TOKEN
r = redis.from_url(os.getenv("REDIS_URL"))
       

@app.route('/api/v1/register', methods=['POST'])
def respondStart():
    chat_id = request.json['message']['chat']['id']
    name = request.json['message']['chat']['first_name']
    userText = request.json['message']['text']
    users = [i.decode() for i in r.keys()]
    password = os.getenv('APP_PASSWORD')
    if "/start" in userText:
        data = {
            "chat_id": chat_id,
	        "text": "Welcome to test bot, %s \nPlease use /login to log in and receive updates." % name
        }
        requests.post(URL + "/sendMessage", data = data)
    elif "/login" in userText:
        if str(chat_id) in users:
            error = {
                "chat_id": chat_id,
                "text": "You're already logged in. Please wait for next notifications or use /getstatus for latest build"
            }
            requests.post(URL + "/sendMessage", data = error)
        else:
            msg = {
                "chat_id": chat_id,
                "text": "Please input password provided by Administrator"
            }
            requests.post(URL + "/sendMessage", data = msg)
    elif str(password) in userText:
        if str(chat_id) in users:
            error = {
                "chat_id": chat_id,
                "text": "You're already logged in. Please wait for next notifications or use /getstatus for latest build"
            }
            requests.post(URL + "/sendMessage", data = error)
        else:
            r.set(chat_id, name)
            msg = {
                "chat_id": chat_id,
                "text": "Authentificated succesfully. Now you'll start receiving notifications"
            }
            time.sleep(1)
            requests.post(URL + "/sendMessage", data = msg)
    else:
        if str(chat_id) in users:
            error = {
                "chat_id": chat_id,
                "text": "Something went wrong! Please use the commands from command pallete:\n - /login \n - /contactus \n - /getstatus \n If you encountered other issues contact administrator and provide this ticket number: " + str(chat_id)
            }
            print(chat_id)
            requests.post(URL + "/sendMessage", data = error)
        else:
            msg = {
                "chat_id": chat_id,
                "text": "You are not logged in. Please enter password to continue"
            }
            requests.post(URL + "/sendMessage", data = msg)
    return Response(status=200)
    



@app.route('/', methods=['GET'])
def getDashboard():
    return render_template("dashboard.html"), 200


@app.route('/petclinic', methods=['GET'])
def getPetclinic():
    def convertTime(epoch):
        result = datetime.datetime.utcfromtimestamp(float(epoch)/1000.).strftime('%Y-%m-%d %H:%M:%S')
        return result
    failed = u'\U0000274C'
    success = u'\U00002705'
    app.jinja_env.globals.update(convertTime=convertTime,success=success,failed=failed)
    try:
        msg = requests.get('https://jenkins.test/job/petclinic_sguzun/job/master/api/json?tree=allBuilds[id,result,url,timestamp]', timeout=1, auth=('sguzun', JENKINS_TOKEN)).json()['allBuilds'][:10]
    except (json.decoder.JSONDecodeError,requests.exceptions.Timeout, requests.exceptions.ConnectionError) as err:
        return render_template("error.html"), 408
    else:
        return render_template("petclinic.html", data=msg), 200



@app.route('/bookscatalog', methods=['GET'])
def getBooksCatalog():
    def convertTime(epoch):
        result = datetime.datetime.utcfromtimestamp(float(epoch)/1000.).strftime('%Y-%m-%d %H:%M:%S')
        return result
    failed = u'\U0000274C'
    success = u'\U00002705'
    app.jinja_env.globals.update(convertTime=convertTime,success=success,failed=failed)
    try:
        msg = requests.get('https://jenkins.${SERVER_URL}/job/bookscatalog_sguzun/job/master/api/json?tree=allBuilds[id,result,url,timestamp]', timeout=1, auth=('sguzun', JENKINS_TOKEN)).json()['allBuilds'][:10]
    except (json.decoder.JSONDecodeError,requests.exceptions.Timeout, requests.exceptions.ConnectionError) as err:
        return render_template("error.html"), 404
    else:
        return render_template("bookscatalog.html", data=msg), 200




@app.route('/api/v1/sendStatus', methods=['POST']) 
def sendStatus():
    if request.args.get('token') == os.environ.get('API_TOKEN') and r.keys():
        sendtry(request.args.get('project'),request.args.get('status'),request.args.get('buildnumber'))
        return Response(status=200)
    elif not r.keys():
        return jsonify({"status": 404, "summary": "Ids not found"}), 404
    else:
        return jsonify({"status": 401, "summary": "Invalid Token"}), 401




def sendtry(project, status, buildNumber):
    logo = ["https://www.jenkins.io/images/logos/general/256.png","https://www.jenkins.io/images/logos/jenkins-x/jenkins-x-256.png","https://www.jenkins.io/images/logos/stay-safe/256.png"]
    robot = u'\U0001F916'
    failed = u'\U0000274C'
    success = u'\U00002705'
    status = f"Project: {project} \nBuild Number: {buildNumber} \nStatus: {status} {failed if status == 'FAILED' else success} \n\n<a href='https://jenkins.${SERVER_URL}/job/{project}/job/master/{buildNumber}/console'>Access console</a> \n\n<b>With {robot} from me</b>"
    for i in r.keys():
        msg = {
            "chat_id": i.decode(),
            "photo": random.choice(logo),
            "caption": str(status),
            "parse_mode": "html"
        }
        requests.post(URL + "/sendPhoto", data = msg)


